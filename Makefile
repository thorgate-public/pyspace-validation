# PYTHON specifies the python binary to use when creating virtualenv
PYTHON ?= python3.9

# Editor can be defined globally but defaults to nano
EDITOR ?= nano

# By default we open the editor after copying settings, but can be overridden
#  (e.g. EDIT_SETTINGS=no make settings).
EDIT_SETTINGS ?= yes

PYTHON_VERSION ?= 3.10
EXECUTOR_IMAGE_SOURCE ?= python:$(PYTHON_VERSION)-slim
EXECUTOR_IMAGE ?= pyspace-validation-executor
EXECUTOR_IMAGE_TAG ?= latest

SERVICE_IMAGE ?= pyspace-validation-service
SERVICE_IMAGE_TAG ?= latest

PORT ?= 5000

# Get root dir and project dir
PROJECT_ROOT ?= $(CURDIR)

BLACK ?= \033[0;30m
RED ?= \033[0;31m
GREEN ?= \033[0;32m
YELLOW ?= \033[0;33m
BLUE ?= \033[0;34m
PURPLE ?= \033[0;35m
CYAN ?= \033[0;36m
GRAY ?= \033[0;37m
COFF ?= \033[0m

.env:
	@cp .env.example .env

.PHONY:
env: .env

.PHONY:
setup: env build-executor


.PHONY:
clean:
	@echo -e "$(CYAN)Cleaning pyc files$(COFF)"
	@find . -name "*.pyc" -exec rm -rf {} \;


.PHONY:
test-py: clean
	poetry run py.test $(cmd)

.PHONY:
test: test-py

.PHONY:
coverage-py: clean
	@make test-py cmd="--cov=pyspace_validation --cov-report html --cov-report xml --cov-report term-missing $(cmd)"

.PHONY:
coverage: coverage-py

.PHONY:
isort:
	poetry run isort .

.PHONY:
isort-check:
	poetry run isort . --check --diff

.PHONY:
black:
	poetry run black .

.PHONY:
black-check:
	poetry run black . --check --diff

.PHONY:
prospector:
	poetry run prospector

.PHONY:
mypy:
	poetry run mypy .


.PHONY:
fmt-py: black isort

.PHONY:
fmt: fmt-py

.PHONY:
format: fmt

.PHONY:
lint-py: black-check isort-check mypy prospector

.PHONY:
lint: lint-py


.PHONY:
start-dev:
	@poetry run adev runserver pyspace_validation --port=$(PORT)

.PHONY:
start:
	@poetry run pyspace-validation runserver --port=$(PORT)

.PHONY:
client:
	@poetry run pyspace-validation $(cmd)

.PHONY:
validate-solution:
	@poetry run pyspace-validation validate-solution $(cmd)


.PHONY:
build-executor:
	@docker build . -f Dockerfile.executor --tag $(EXECUTOR_IMAGE):$(EXECUTOR_IMAGE_TAG)


.PHONY:
build-service:
	@docker build . -f Dockerfile.production --tag $(SERVICE_IMAGE):$(SERVICE_IMAGE_TAG)
