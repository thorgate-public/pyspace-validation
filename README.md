# PySpace validator


## Requirements
    
- Python 3.9+
- Docker
- Poetry

## Basic usage

```shell
# install dependencies
poetry install

# Copy .env and build/pull base image
make setup

# Start application server
make start-dev # or start-dev to start development version

# In another terminal call
make validate-solution cmd=--help
# or `poetry run python cli.py validate-solution --help`
```

`validate-solution` can read options from `config.json`

Example `config.json`

```json
{
    "validation_server": "http://localhost:5000",
    "payload": {
        "files": {
            "solution.py": "/path/to/solution.py",
            "tester.py": "/path/to/tester.py"
        },
        "solution_function": "function_name_to_test"
    }
}
```

To add extra `requirements` then add path to requirements file to the payload.

## Swagger API

Swagger documentation can be viewed from `http://localhost:5000/api`


## Usage with REST

To use validation API with REST POST your solution and tester to `/api/validate`

Files are expected to be string values, so you need to read the files in first.

Example payload:
```json
{
    "files": {
        "solution.py": "...",
        "tester.py": "..."
    },
    "solution_function": "function_name_to_test"
}
```


## Usage with WebSockets

To use validation API with WebSockets, send your solution and tester to `/api/validate/ws`.

The process expects validation payload to be sent first, if no solution is sent within 5 seconds, server will close the connection.
After solution is sent, server will wait for the solution checking to finish.
Server will also send alive messages every 10 seconds while the solution is being checked to keep the connection active.


## Validation payload

Validation payload is expected to contain following elements.
If there is any issues with payload then Swagger API shows correct payload format.

### `files`

Type: **object**
Required: **yes**

An object where keys are file names and value is the file contents encoded with base64.
If filename contains `/` then anything leading to last section is treated as directory.

Base64 encoding on files allows binary files to be uploaded as test data.

`tester.py` and `solution.py`  are required entries.


### `solution_function`

Type: **string**
Required: **yes**

Solution function name that is expected to exist in `solution.py`


### `requirements`

Type: **string**
Required: **no**

Extra requirements to be installed for solution validation. Note: Custom docker image will be used for this.

Example:
```text
requests
pandas
```


### `network_allowed`

Type: **bool**
Required: **no**

Define if connection to internet allowed during validation or not.


### `timeout`

Type: **number**
Required: **no**

Define maximum execution time (in seconds) of testing. If execution exceeds that then `status=15` is returned.


### `callback_url`

Type: **string**
Required: **not required for WebSockets**

Used with REST calls, defines URL used to report results to.


## FAQ

### Using custom Docker host

Define your docker host environment variable `DOCKER_HOST`.

E.g:
```shell
export DOCKER_HOST=http://docker:2375
```


## Contributing

Contributions are very welcome. Tests can be run with `make test`, please ensure the coverage at least stays the same before you submit a pull request.

### Get Started!

Ready to contribute? Here's how to set up pyspace-validation for local
development.

1. Fork the pyspace-validation repo on Gitlab.
2. Clone your fork locally:

        $ git clone git@gitlab.com:your_name_here/pyspace-validation.git

3. Install dependencies

        $ poetry install

4. Create a branch for local development:

        $ git checkout -b name-of-your-bugfix-or-feature

    Now you can make your changes locally.

5. When you're done making changes, check that your the tests and linting
    still pass.

        $ make format
        $ make lint
        $ make test

6. Commit your changes and push your branch to Gitlab:

        $ git add .
        $ git commit -m "Your detailed description of your changes."
        $ git push origin name-of-your-bugfix-or-feature

7. Submit a pull request through the Gitlab website.
