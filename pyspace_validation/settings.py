import os

from environs import Env


PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))

env = Env()
env.read_env()

DEBUG = env.bool("VALIDATION_DEBUG", default=False)

SECRET_KEY = env.str("VALIDATION_SECRET_KEY")

VALIDATION_DOCKER_IMAGE = env.str("VALIDATION_DOCKER_IMAGE")

# Directory within validation worker
DATA_DIR = env.str("VALIDATION_DATA_DIR")

# Mapping docker host paths
DOCKER_HOST_DIR = env.str("VALIDATION_DOCKER_HOST_DIR", default=None)

DOCKER_REGISTRY_USER = env.str("VALIDATION_DOCKER_REGISTRY_USER", default=None)
DOCKER_REGISTRY_TOKEN = env.str("VALIDATION_DOCKER_REGISTRY_TOKEN", default=None)

MAXIMUM_TIMEOUT = env.int("VALIDATION_MAXIMUM_TIMEOUT", default=30)

WORKER_COUNT = env.int("VALIDATION_WORKER_COUNT", default=10)

REPORT_RETRY_COUNT = env.int("VALIDATION_REPORT_RETRY_COUNT", default=10)
