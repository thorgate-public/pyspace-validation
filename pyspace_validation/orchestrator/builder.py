import asyncio
import logging
import tarfile
import tempfile
import typing
from io import BytesIO
from typing import BinaryIO

from aiodocker import Docker

from pyspace_validation.exceptions import DockerBuildError


logger = logging.getLogger(__name__)

docker_file = """
FROM {image_base}

USER root

# Task specific requirements
COPY ./requirements.txt /src/requirements-task.txt

RUN pip install -r requirements-task.txt

USER 1000:1000
"""


def add_bytesio_to_file(tar_file_obj: tarfile.TarFile, filename: str, file_obj: BytesIO):
    """
    Add BytesIO contents to tarfile.

    Args:
        tar_file_obj: tarfile object
        filename: Filename to use for archive filename
        file_obj: File object to be added to tarfile
    """
    df_info = tarfile.TarInfo(filename)
    df_info.size = len(file_obj.getvalue())
    file_obj.seek(0)
    tar_file_obj.addfile(df_info, file_obj)


def make_dockerfile_archive(image: str, requirements: bytes) -> BinaryIO:
    """
    Create Tar archive from Docker file template and requirements.

    Args:
        image: Base image to build custom image from.
        requirements: Extra requirements file contents.

    Returns: Binary IO object containing docker file archive.
    """
    docker_file_tmp = docker_file.format(image_base=image)

    # Create BinaryIO objects containing file contents for the archive
    docker_file_obj = BytesIO(docker_file_tmp.encode("utf-8"))
    requirements_obj = BytesIO(requirements)

    with tempfile.NamedTemporaryFile() as fp:
        # Add Docker file(s) to archive
        with tarfile.open(mode="w:gz", fileobj=fp) as tar:
            add_bytesio_to_file(tar, "Dockerfile", docker_file_obj)
            add_bytesio_to_file(tar, "requirements.txt", requirements_obj)

        fp.seek(0)

        return BytesIO(fp.read())


def format_build_log(build_log: list[dict[str, typing.Any]]) -> str:
    return "".join(
        [
            entry["error"] if "error" in entry else entry["stream"]
            for entry in build_log
            if "error" in entry or "stream" in entry
        ]
    )


async def build_custom_image(docker: Docker, hash_id: str, image: str, requirements: bytes) -> str:
    """
    Build custom Docker image and remove.

    Args:
        docker: Docker API client instance.
        hash_id: Solution ID.
        image: Base docker image for the template.
        requirements: Extra requirements file contents.

    Returns: Custom image name.
    """
    loop = asyncio.get_running_loop()

    # Create in-memory tar file with docker building context archive
    tar_obj = await loop.run_in_executor(None, make_dockerfile_archive, image, requirements)

    image_name = f"pyspace-validation-executor-{hash_id}:latest"

    # Build custom image from the archive
    build_result: list[dict[str, typing.Any]] = []

    async for result_line in docker.images.build(
        fileobj=tar_obj,
        tag=image_name,
        labels={
            "ee.thorgate.project": "PySpace Validation Executor",
            "ee.pyspace.executor.type": "custom",
        },
        encoding="utf-8",
        stream=True,
    ):
        build_result.append(result_line)

    build_log = format_build_log(build_result)
    logger.debug("Docker image build result: %s", build_log)

    if build_result[-1].get("error"):
        raise DockerBuildError(f"Failed to build custom image: {build_result[-1].get('error')}", build_log)

    # Ensure image has been built - if not then this raises DockerError
    await docker.images.inspect(image_name)

    return image_name
