import asyncio
import base64
import binascii
import logging
import os
import shutil
from contextlib import asynccontextmanager
from pathlib import Path
from typing import Optional

import aiofiles
import aiofiles.os
from aiodocker import Docker, DockerError
from jwt import decode

from .. import settings
from ..exceptions import DecodingError, InvalidInputPath
from ..types import CodeDirectory, ContainerStats, ValidationPayload, ValidationPayloadFiles, ValidationReport
from .builder import build_custom_image


logger = logging.getLogger(__name__)


async def ensure_image(docker: Docker, image: str) -> str:
    """
    Ensure Docker image exists

    Args:
        docker: Docker API client instance.
        image: Image name including repository.

    Returns: Image name if valid.

    Raises: DockerError if image does not exist or pulling is not configured.
    """
    try:
        logger.debug("Ensure docker image '%s' exists...", image)
        await docker.images.inspect(image)
    except DockerError:
        auth = None
        if settings.DOCKER_REGISTRY_USER and settings.DOCKER_REGISTRY_TOKEN:
            logger.debug("Trying to pull docker image '%s' with credentials", image)
            token = base64.b64encode(
                f"{settings.DOCKER_REGISTRY_USER}:{settings.DOCKER_REGISTRY_TOKEN}".encode(),
            ).decode()
            auth = {"auth": token}

        await docker.pull(
            image,
            auth=auth,
        )

    return image


async def decode_result(secret_key: str, logs: list[str]) -> Optional[ValidationReport]:
    """
    Parse exported PyTest JSON Report from the validator.

    Args:
        secret_key: Hashing key used to verify the report.
        logs: Docker container logs to parse.

    Returns: [ValidationReport] dict if valid, otherwise [None].

    """
    token = ""
    next_is_valid = False
    for log in logs:
        if next_is_valid:
            token += log

        # Keep this in sync with: executor/runner.py:32
        if "+------<<<<        TEST REPORT        >>>>------+" in log:
            next_is_valid = True

    if not token:
        return None

    # Strip new line from the token - invalidates the payload and parsing fails
    if token.endswith("\n"):
        token = token[:-1]

    loop = asyncio.get_running_loop()

    return await loop.run_in_executor(None, decode, token, secret_key, ["HS256"])


def decode_file_payload(file_contents: str) -> bytes:
    """
    Decode base64 encoded file contents

    Args:
        file_contents: Encoded file contents.

    Returns: File contents in bytes

    """

    try:
        return base64.b64decode(file_contents)

    except binascii.Error as e:
        raise DecodingError() from e


async def write_files(temp_dir: CodeDirectory, files: ValidationPayloadFiles) -> list[str]:
    """
    Write files to temporary directory.

    Args:
        temp_dir: Temporary directory name.
        files: Dict of payload files, key is file name and value is mapped file contents encoded with base64.

    Returns:

    """
    bind_mounts: set[str] = set()

    mount_root = Path("/src")
    temp_root = Path(temp_dir.full_dir)

    logger.debug("Temp dir root: '%s'", temp_root)

    for file_name, file_contents in files.items():
        remote_path = (mount_root / file_name).resolve()
        local_path = (temp_root / file_name).resolve()

        mount_path = local_path
        if temp_dir.host_dir:
            mount_path = (temp_dir.host_dir / file_name).resolve()

        if not remote_path.is_relative_to(mount_root):
            raise InvalidInputPath("Invalid input paths")

        if mount_root != remote_path.parent:
            exists = await aiofiles.os.path.exists(local_path.parent)

            if not exists:
                await aiofiles.os.makedirs(local_path.parent)

        bind_mounts.add(f"{mount_path}:{remote_path}:ro")

        async with aiofiles.open(local_path, mode="wb") as afp:
            await afp.write(decode_file_payload(file_contents))

    return list(bind_mounts)


# pylint: disable=too-many-arguments
async def run_container(
    docker: Docker,
    image: str,
    bind_mounts: list[str],
    payload: ValidationPayload,
    stats: ContainerStats,
    logs: list[str],
) -> None:
    """
    Run container for solution validation.
    Also handles stats collection and timeouts.

    Args:
        docker: Docker API client instance.
        image: Docker image name to start the container with.
        bind_mounts: Bind mounts for the container
        payload: Validation payload.
        stats: List to where container stats will be stored to.
        logs: List to where container logs will be stored to.

    Returns: Tuple of container logs and stats.
    """
    solution_function = payload.get("solution_function", "")
    timeout = payload["timeout"]

    logger.info("Creating container for image '%s'", image)
    container = await docker.containers.create(
        {
            "Env": [
                f"SECRET_KEY={settings.SECRET_KEY}",
                f"TEST_FUNCTION={solution_function}",
            ],
            "Labels": {
                "ee.thorgate.pyspace": "True",
            },
            "Cmd": f"{timeout}",
            "Image": image,
            "AutoRemove": True,
            "NetworkDisabled": not payload["network_allowed"],
            "HostConfig": {
                "Binds": bind_mounts,
            },
        }
    )
    logger.debug("Creating container for image '%s': DONE", image)

    async def collect_container_logs():
        async for data in container.log(stdout=True, follow=True):
            logs.append(data)

        logger.info("Finished container '%s' logs collection", container.id)

    async def collect_container_stats():
        async for data in container.stats(stream=True):
            stats.append(data)

        logger.info("Finished container '%s' stats collection", container.id)

    async def kill_container():
        # Timeout is applied to each stage: setup + call + teardown
        await asyncio.sleep(3 * timeout + 1)
        await container.kill()
        raise TimeoutError()

    logger.info("Starting container '%s'", container.id)
    await container.start()

    # Start processing tasks to collect
    logs_task = asyncio.create_task(collect_container_logs())
    stats_task = asyncio.create_task(collect_container_stats())
    fallback_kill_task = asyncio.create_task(kill_container())

    try:
        logger.info("Waiting container '%s' to finish", container.id)
        # Wait for container to exit or be killed
        await container.wait()
        logger.info("Container '%s' finished", container.id)

    finally:
        # Stop fallback task if it is still running
        fallback_kill_task.cancel()
        logs_task.cancel()
        stats_task.cancel()

    # Wait for tasks to finish
    await asyncio.gather(logs_task, stats_task, return_exceptions=True)


@asynccontextmanager
async def code_directory(hash_id: str):
    """
    Context manager to create directory for solution code.

    Args:
        hash_id: Solution ID.

    Yields: Solution directory
    """
    loop = asyncio.get_running_loop()

    root_dir = Path(settings.DATA_DIR)

    # ensure that root_dir is always absolute to validation container/host
    if not root_dir.is_absolute():
        root_dir = Path(settings.PROJECT_ROOT) / root_dir

    docker_host_dir = None
    if settings.DOCKER_HOST_DIR:
        docker_host_dir = Path(settings.DOCKER_HOST_DIR) / hash_id

    # Create solution data directory
    await loop.run_in_executor(None, os.makedirs, root_dir / hash_id)

    yield CodeDirectory(
        base_dir=hash_id,
        full_dir=root_dir / hash_id,
        host_dir=docker_host_dir,
    )

    # Remove the solution data directory
    await loop.run_in_executor(None, shutil.rmtree, root_dir / hash_id)


@asynccontextmanager
async def use_custom_docker_image(docker: Docker, hash_id: str, requirements: Optional[str]):
    """
    Context manager to use custom docker image if requirements are defined.
        If custom docker image is used it will be removed when the context manager exits.

    Args:
        docker: Docker API client instance.
        hash_id: Solution ID.
        requirements: Base64 encoded requirements contents or None

    Yields: Docker image to use for the executor.
    """
    custom_image = False
    docker_image = await ensure_image(docker, settings.VALIDATION_DOCKER_IMAGE)

    if requirements:
        docker_image = await build_custom_image(docker, hash_id, docker_image, decode_file_payload(requirements))
        custom_image = True

    yield docker_image

    if custom_image:
        await docker.images.delete(docker_image, force=True)
