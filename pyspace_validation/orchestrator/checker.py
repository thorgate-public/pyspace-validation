import logging
import typing

from aiodocker import Docker, DockerError

from .. import settings
from ..exceptions import DecodingError
from ..types import (
    ContainerStats,
    ResultStatus,
    ValidationPayload,
    ValidationReport,
    ValidationResult,
    ValidationSummary,
    ValidationSummaryTests,
)
from .utils import code_directory, decode_result, run_container, use_custom_docker_image, write_files


logger = logging.getLogger(__name__)


def collect_tests(report: ValidationReport):
    """
    Collect validation report tests.

    Args:
        report: Validation report to format.

    Returns: Generator object of validation summaries.
    """
    for test in report["tests"]:
        __, name = test["nodeid"].rsplit("::", 1)

        test_call = test.get("call")

        if test_call:
            duration = test["call"]["duration"]
        else:
            duration = 0

        yield ValidationSummaryTests(
            name=name,
            outcome=test["outcome"],
            duration=duration,
        )


def process_report(
    report: typing.Optional[ValidationReport],
    stats: ContainerStats,
    logs: typing.Optional[list[str]],
) -> ValidationResult:
    """
    Process validation report to expected output format.

    Args:
        report: Validation report
        stats: Container stats.
        logs: Container logs.

    Returns: Validation result payload
    """

    if not report:
        return ValidationResult(
            status=ResultStatus.WRONG_RESULT,
            reason="Report was not recovered",
            report=None,
            summary=None,
            stats=stats,
            logs=logs,
        )

    total = report["summary"]["total"]
    failed = report["summary"].get("failed", 0)
    passed = report["summary"].get("passed", 0)

    summary = ValidationSummary(
        error=report["summary"].get("error", 0),
        failed=failed,
        passed=passed,
        total=report["summary"]["total"],
        duration=report["duration"],
        tests=list(collect_tests(report)),
    )

    if failed == 0 and passed == total:
        result_status = ResultStatus.CORRECT_RESULT
    else:
        result_status = ResultStatus.WRONG_RESULT

    return ValidationResult(
        status=result_status,
        reason=None,
        summary=summary,
        report=report,
        stats=stats,
        logs=logs,
    )


async def check_solution(
    hash_id: str,
    payload: ValidationPayload,
) -> ValidationResult:
    """
    Check the solution in following order.
        - build custom image if needed
        - run the container with the solution code
        - validate the report and process it

    Args:
        hash_id: Solution ID.
        payload: Validation payload

    Returns: Payload containing solution result. On errors report with invalid status is returned.
    """
    async with Docker() as docker:
        stats: ContainerStats = []
        logs: list[str] = []

        try:
            async with code_directory(hash_id) as temp_dir:
                async with use_custom_docker_image(docker, hash_id, payload["requirements"]) as image:
                    bind_mounts = await write_files(temp_dir, payload["files"])

                    await run_container(
                        docker=docker,
                        image=image,
                        bind_mounts=bind_mounts,
                        payload=payload,
                        stats=stats,
                        logs=logs,
                    )

                    report_logs = logs if settings.DEBUG else None

                    report = await decode_result(settings.SECRET_KEY, logs)
                    result = process_report(
                        report=report,
                        stats=stats,
                        logs=report_logs,
                    )

                    return result
        except TimeoutError as e:  # pragma: nocover
            logger.exception("Timeout error: %s", str(e), exc_info=True)
            return ValidationResult(
                status=ResultStatus.TIMEOUT,
                reason="Execution timed out",
                report=None,
                summary=None,
                stats=[],
                logs=report_logs,
            )

        except DockerError as e:  # pragma: nocover
            logger.exception(str(e), exc_info=True)
            return ValidationResult(
                status=ResultStatus.WRONG_RESULT,
                reason=str(e),
                report=None,
                summary=None,
                stats=[],
                logs=report_logs,
            )

        except DecodingError as e:  # pragma: nocover
            logger.exception(str(e), exc_info=True)
            return ValidationResult(
                status=ResultStatus.WRONG_RESULT,
                reason=f"Error decoding file: {e}",
                report=None,
                summary=None,
                stats=[],
                logs=report_logs,
            )

        except Exception as e:  # pragma: nocover
            logger.exception(str(e), exc_info=True)
            return ValidationResult(
                status=ResultStatus.WRONG_RESULT,
                reason=str(e),
                report=None,
                summary=None,
                stats=[],
                logs=report_logs,
            )
