import asyncio
import logging
from types import SimpleNamespace

from aiohttp import ClientSession, TraceConfig, TraceRequestExceptionParams, TraceRequestStartParams
from aiohttp.web import Application
from aiohttp_retry import ExponentialRetry, RetryClient

from .. import settings
from ..types import ValidationQueuePayload, ValidationResult
from .checker import check_solution


logger = logging.getLogger(__name__)
retry_options = ExponentialRetry(attempts=settings.REPORT_RETRY_COUNT)


async def on_request_start(
    session: ClientSession,
    trace_config_ctx: SimpleNamespace,
    params: TraceRequestStartParams,
) -> None:  # pragma: nocover
    current_attempt = trace_config_ctx.trace_request_ctx["current_attempt"]
    if retry_options.attempts <= current_attempt:
        logger.exception("Maximum retries reached for payload")


async def on_request_exception(
    session: ClientSession,
    trace_config_ctx: SimpleNamespace,
    params: TraceRequestExceptionParams,
):  # pragma: nocover
    logger.exception("Reporting request error: %s", params.exception, exc_info=True)


async def report_result(url: str, payload: ValidationResult):
    """
    Helper to report solution status. Automatically retries up to configured count.

    If maximum retries is reached, an error is logged.

    Args:
        url: URL to send the callback to.
        payload: Payload to send.

    """
    trace_config = TraceConfig()
    trace_config.on_request_start.append(on_request_start)
    trace_config.on_request_exception.append(on_request_exception)

    async with RetryClient(retry_options=retry_options, trace_configs=[trace_config]) as client:
        await client.post(url, json=payload)


async def worker(worker_id: int, queue: asyncio.Queue[ValidationQueuePayload]):
    """
    Validation queue worker task.

    Args:
        worker_id:
        queue: asyncio.Queue instance to read payloads from.

    """
    try:
        logger.info("Starting validation worker: %s", worker_id)

        while True:
            # Get a "work item" out of the queue.
            item = await queue.get()

            payload = item["payload"]
            callback_url = item["callback_url"]

            result = await check_solution(hash_id=item["hash_id"], payload=payload)

            # Report solution status
            await report_result(callback_url, result)

            # Notify the queue that the "work item" has been processed.
            queue.task_done()
    finally:
        logger.info("Stopping validation worker: %s", worker_id)


async def setup_workers(app: Application):
    """
    Setup AsyncIO Queue and worker consumers for validation payloads.
        This is a context manager to also handle cleanup of the remaining tasks.

    Args:
        app: AioHttp application instance
    """
    app["validation_queue"] = asyncio.Queue()

    # Start worker tasks
    app["validation_queue_tasks"] = []
    for i in range(settings.WORKER_COUNT):
        task = asyncio.create_task(worker(i, app["validation_queue"]))
        app["validation_queue_tasks"].append(task)

    # Wait for app to run
    yield

    # Wait for queue to empty
    await app["validation_queue"].join()

    # Cancel our worker tasks.
    for task in app["validation_queue_tasks"]:
        task.cancel()

    # Wait until all worker tasks are cancelled.
    await asyncio.gather(*app["validation_queue_tasks"], return_exceptions=True)
