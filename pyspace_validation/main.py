import asyncio
import base64
import json
import logging.config
import os
import typing
from pprint import pprint

import click
from aiohttp import ClientSession, web

from . import settings
from .application import app_factory
from .application.validation import validate_payload_base
from .logging_config import setup_logging_queue


logging.config.dictConfig(
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {"format": "%(asctime)s [%(levelname)s] %(name)s:%(lineno)d %(funcName)s - %(message)s"},
        },
        "handlers": {"console": {"class": "logging.StreamHandler", "formatter": "default"}},
        "loggers": {
            "": {"handlers": ["console"], "level": "DEBUG" if settings.DEBUG else "INFO"},
        },
    }
)


__version__ = "1.0.0"
__version_info__ = tuple([int(num) for num in __version__.split(".")])  # noqa

DEFAULT_CONFIG_FILE = "config.json"
DEFAULT_SERVER = "ws://127.0.0.1:5000"


async def check_solution(validation_server: str, payload: dict):
    async with ClientSession() as session:
        async with session.ws_connect(f"{validation_server}/api/validate/ws") as ws:
            await ws.send_json(payload)

            result = await ws.receive_json()
            pprint(result)


def read_file(file_name: typing.Optional[str]) -> typing.Optional[str]:
    if not file_name:
        return None

    with open(file_name, encoding="utf-8") as fp:
        return base64.b64encode(fp.read().encode("utf-8")).decode("utf-8")


@click.group()
@click.version_option(version=__version__)
def main():
    setup_logging_queue()


@main.command()
@click.option("-p", "--port", type=int, default=5000)
def runserver(port: int):
    app = app_factory()
    web.run_app(app, host="0.0.0.0", port=port)


@main.command()
@click.option("-c", "--config-file", default=DEFAULT_CONFIG_FILE, show_default=True)
@click.option("-vs", "--validation-server", default=DEFAULT_SERVER, show_default=True)
@click.option("-s", "--solution-path", required=False)
@click.option("-t", "--tester-path", required=False)
@click.option("-r", "--requirements-path", required=False)
@click.option("-fn", "--solution-function", default="", required=False)
# pylint: disable=too-many-arguments
def validate_solution(
    config_file: str,
    validation_server: str,
    solution_path: str,
    tester_path: str,
    requirements_path: str,
    solution_function: str,
):
    input_files = {
        "solution.py": read_file(solution_path),
        "tester.py": read_file(tester_path),
    }

    config = {
        "files": input_files,
        "solution_function": solution_function,
        "requirements": read_file(requirements_path),
    }
    if os.path.exists(config_file):
        with open(config_file, encoding="utf-8") as fp:
            config_file_contents = json.load(fp)

            if "validation_server" in config_file_contents:
                validation_server = config_file_contents["validation_server"]

            input_payload = config_file_contents.get("payload", {})

            files = {
                **input_files,
                **input_payload["files"],
            }

            for key, value in files.items():
                files[key] = read_file(value)

            config = {
                **config,
                **input_payload,
                "requirements": read_file(input_payload.get("requirements")),
                "files": files,
            }

    payload = validate_payload_base(config)

    asyncio.run(
        check_solution(
            validation_server,
            payload,
        )
    )


if __name__ == "__main__":
    main()
