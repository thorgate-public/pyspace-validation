import json
from typing import Any, Optional


class InvalidInputPath(Exception):
    pass


class DockerBuildError(Exception):
    pass


class DecodingError(Exception):
    pass


class ValidationError(Exception):
    def __init__(self, error: str, *args: object, errors: Optional[dict[str, Any]] = None, status=400) -> None:
        super().__init__(*args)

        self.error = error
        self.errors = errors
        self.status = status

    @property
    def data(self):
        result: dict[str, Any] = {"error": self.error}

        if self.errors:
            result["errors"] = self.errors

        return result

    def serialize(self) -> str:
        return json.dumps(self.data)
