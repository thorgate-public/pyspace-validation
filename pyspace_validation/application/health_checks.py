from typing import Optional

from aiodocker import Docker, DockerError


async def check_docker() -> tuple[bool, Optional[str]]:
    """
    Check if docker API is reachable.

    Returns: Tuple of boolean and string. This indicates if docker API is reachable or not.
    """
    docker = Docker()

    try:
        await docker.version()
        return True, None
    except DockerError:
        return False, "Cannot connect to docker engine"
    finally:
        await docker.close()
