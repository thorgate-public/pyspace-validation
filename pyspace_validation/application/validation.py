import json
from typing import Any

import fastjsonschema
from aiohttp.web import Request

from .. import settings
from ..exceptions import ValidationError
from ..types import ValidationPayload


SCHEMA = {
    "type": "object",
    "properties": {
        "files": {
            "type": "object",
            "properties": {"tester.py": {"type": "string"}},
            "additionalProperties": True,
            "required": ["tester.py"],
        },
        "solution_function": {
            "type": "string",
        },
        "requirements": {"type": ["string", "null"], "default": None},
        "network_allowed": {
            "type": "boolean",
            "default": False,
        },
        "timeout": {
            "type": "number",
            "default": 10,
            "minimum": 0.1,
            "maximum": settings.MAXIMUM_TIMEOUT,
        },
        "callback_url": {"type": ["string", "null"], "default": None},
    },
    "required": ["files"],
}

validate_payload_base = fastjsonschema.compile(SCHEMA)


async def validate_payload(data: dict[str, Any]) -> ValidationPayload:
    try:
        return validate_payload_base(data)
    except fastjsonschema.JsonSchemaValueException as e:
        raise ValidationError(
            f"Validation error: {e.message}",
            errors={
                "reason": e.message,
                "key": e.name,
                "path": e.path,
            },
        ) from e


async def validate_request(request: Request) -> ValidationPayload:
    try:
        contents = await request.json()

        return await validate_payload(contents)
    except (json.decoder.JSONDecodeError, TypeError) as e:
        raise ValidationError(
            "Request is malformed; could not decode JSON object.",
        ) from e
