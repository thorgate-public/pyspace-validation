from weakref import WeakSet

from aiohttp import web, WSCloseCode
from aiohttp_swagger import setup_swagger

from ..orchestrator import setup_workers
from .handlers import health_check_handler, rest_handler, websocket_handler
from .validation import SCHEMA


async def on_shutdown(app: web.Application):
    for ws in set(app["websockets"]):
        await ws.close(code=WSCloseCode.GOING_AWAY, message="Server shutdown")


def app_factory():
    app = web.Application()

    app["websockets"] = WeakSet()

    app.cleanup_ctx.append(setup_workers)
    app.on_shutdown.append(on_shutdown)

    # Add handlers
    app.router.add_get("/_health", health_check_handler)
    app.router.add_post("/api/validate", rest_handler)
    app.router.add_get("/api/validate/ws", websocket_handler)

    setup_swagger(
        app,
        swagger_url="/api/doc",
        ui_version=2,
        definitions={
            "ValidationPayload": SCHEMA,
            "ValidationResult": {
                "type": "object",
                "properties": {
                    "status": {
                        "type": "integer",
                    },
                    "reason": {
                        "type": ["string", "null"],
                        "default": None,
                    },
                    "report": {
                        "type": "object",
                        "additionalProperties": True,
                    },
                    "summary": {
                        "type": "object",
                        "properties": {
                            "failed": {"type": "number"},
                            "error": {"type": "number"},
                            "passed": {"type": "number"},
                            "total": {"type": "number"},
                            "duration": {"type": "number"},
                            "tests": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "outcome": {"type": "string", "enum": ["passed", "failed", "error"]},
                                    "duration": {"type": "number"},
                                },
                            },
                        },
                    },
                    "stats": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "additionalProperties": True,
                        },
                    },
                },
                "required": [
                    "status",
                    "reason",
                    "summary",
                ],
            },
        },
    )

    return app
