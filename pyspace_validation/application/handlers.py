import asyncio
import json
import logging
import uuid

from aiohttp import web
from aiohttp.web import HTTPBadRequest, Request

from .. import settings, types
from ..exceptions import ValidationError
from ..orchestrator import check_solution
from .health_checks import check_docker
from .validation import validate_payload, validate_request


logger = logging.getLogger(__name__)


async def health_check_handler(request: Request):
    """
    ---
    description: This endpoint allow to test that service is up.
    tags:
    - Health check
    produces:
    - application/json
    responses:
        "200":
            description: successful operation, returns docker engine status
            schema:
              type: object
              properties:
                docker:
                  properties:
                    status:
                      type: boolean
                    reason:
                      type: string
                  status:
                  - docker
              required:
              - docker
        "405":
            description: invalid HTTP Method
    """

    docker_up, message = await check_docker()

    return web.json_response(
        {
            "docker": {
                "status": docker_up,
                "reason": message,
            },
        }
    )


async def rest_handler(request: Request):
    """
    ---
    description: "This endpoint accepts validation payload, if `callback_url` is omitted then background queue is not used."
    tags:
    - Validation
    parameters:
      - in: body
        name: "payload"
        schema:
          $ref: '#/definitions/ValidationPayload'
    produces:
    - application/json
    responses:
        "201":
            description: Successful operation, create background task and returns.
            schema:
              type: object
              properties:
                accepted:
                  type: boolean
              required:
              - accepted
        "200":
            description: Successful operation, returns validation result.
            schema:
              $ref: '#/definitions/ValidationResult'
    """
    try:
        payload = await validate_request(request)
    except ValidationError as e:
        raise HTTPBadRequest(
            text=e.serialize(),
            content_type="application/json",
        ) from e

    callback_url = payload["callback_url"]

    if callback_url:
        queue: asyncio.Queue[types.ValidationQueuePayload] = request.app["validation_queue"]
        await queue.put(
            types.ValidationQueuePayload(
                hash_id=str(uuid.uuid4()),
                payload=payload,
                callback_url=callback_url,
            )
        )
        return web.json_response({"accepted": True}, status=201)

    if not settings.DEBUG and not callback_url:
        # Return error to indicate that callback_url is required!
        raise HTTPBadRequest(
            text=json.dumps(
                {
                    "error": "Validation error: {}".format(
                        "data must contain ['files', 'callback_url'] properties",
                    ),
                }
            ),
            content_type="application/json",
        )

    result = await check_solution(hash_id=str(uuid.uuid4()), payload=payload)

    return web.json_response(result)


async def websocket_handler(request: Request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    request.app["websockets"].add(ws)

    try:
        # Wait for validation payload to be sent
        data = await ws.receive_json()
        payload = await validate_payload(data)

        # Validate solution
        result = await check_solution(hash_id=str(uuid.uuid4()), payload=payload)

        # Send response to user
        await ws.send_json(result)
    except ValidationError as e:
        await ws.send_json(e.data)

    except (json.decoder.JSONDecodeError, TypeError):
        await ws.send_json({"error": "Request is malformed; could not decode JSON object."})

    except Exception as e:  # pragma: nocover
        logger.exception(str(e), exc_info=True)
        await ws.send_json({"error": str(e)})

    except asyncio.CancelledError:
        pass

    finally:
        request.app["websockets"].discard(ws)

    logger.debug("websocket connection closed")

    return ws
