"""
Pytest codes:

Exit code 0:	All tests were collected and passed successfully
Exit code 1:	Tests were collected and run but some of the tests failed
Exit code 2:	Test execution was interrupted by the user
Exit code 3:	Internal error happened while executing tests
Exit code 4:	pytest command line usage error
Exit code 5:	No tests were collected

"""
import unittest
import unittest.mock

import pytest

import solution


def mocked_func_usd(name):
    return "$"


def mocked_func_pound(name):
    return "£"


def mocked_func_eur(name):
    return "€"


class TestTest(unittest.TestCase):
    def test_test_passes(self):
        result = pytest.main(["solution.py"])
        self.assertEqual(result, 0)

    def test_test_fails_with_invalid_function_usd(self):
        with unittest.mock.patch.object(solution, "currency_sign_from_name", mocked_func_usd):
            # assert the test fails
            assert (
                pytest.main(["solution.py"]) == 1
            ), "Test also accepts `$`{usd sign} as an acceptable return value for input `euro`"

    def test_test_passes_with_eur(self):
        with unittest.mock.patch.object(solution, "currency_sign_from_name", mocked_func_eur):
            # assert the test passes
            assert (
                pytest.main(["solution.py"]) == 0
            ), "Test fails when function returns the correct return value `€`{eur sign} for input `euro`"

    def test_test_fails_with_invalid_function_pound(self):
        with unittest.mock.patch.object(solution, "currency_sign_from_name", mocked_func_pound):
            # assert the test fails
            assert (
                pytest.main(["solution.py"]) == 1
            ), "Test also accepts `£`{pound sign} as an acceptable return value for input `euro`"
