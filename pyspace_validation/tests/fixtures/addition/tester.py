import pytest


@pytest.fixture
def accepting_data():
    def _add(n1, n2):
        return n1 + n2

    return _add


def test_integers(solution_function, accepting_data):
    numbers_1 = [1, 2, 3, 5, 8, 13, 21]
    numbers_2 = [13, 21, 34, 55, 89, 144]

    for number1 in numbers_1:
        for number2 in numbers_2:
            assert solution_function(number1, number2) == accepting_data(number1, number2)


def test_floats(solution_function, accepting_data):
    numbers_1 = [1.2, 3.5, 8.13, 21.333]
    numbers_2 = [13.21, 34.55, 89.144]

    for number1 in numbers_1:
        for number2 in numbers_2:
            assert solution_function(number1, number2) == accepting_data(number1, number2)
