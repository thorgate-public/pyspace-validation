import json


def test_payload_dict(solution_function):
    payload = {
        "id": 1,
        "name": "Test",
    }

    assert json.loads(solution_function(payload)["data"]) == payload


def test_payload_list(solution_function):
    payload = [
        {
            "id": 1,
            "name": "Test",
        },
    ]

    assert json.loads(solution_function(payload)["data"]) == payload
