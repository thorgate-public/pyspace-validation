import requests


def http_bin_post(data):
    response = requests.post("https://httpbin.org/post", json=data)
    return response.json()
