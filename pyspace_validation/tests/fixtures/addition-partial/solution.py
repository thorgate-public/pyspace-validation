import time


def add(number1, number2):
    return number1 + number2


def add_sleep(number1, number2):
    time.sleep(9)
    return number1 + number2
