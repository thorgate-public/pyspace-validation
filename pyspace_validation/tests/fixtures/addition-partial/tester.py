import os


def test_integers(solution_function):
    numbers_1 = [1, 2, 3, 5, 8, 13, 21]
    numbers_2 = [13, 21, 34, 55, 89, 144]

    def add(n1, n2):
        return n1 - n2

    for number1 in numbers_1:
        for number2 in numbers_2:
            assert solution_function(number1, number2) == add(number1, number2)


def test_floats(solution_function):
    print(os.environ)

    numbers_1 = [1.2, 3.5, 8.13, 21.333]
    numbers_2 = [13.21, 34.55, 89.144]

    def add(n1, n2):
        return n1 + n2

    for number1 in numbers_1:
        for number2 in numbers_2:
            assert solution_function(number1, number2) == add(number1, number2)
