import pytest

from .. import settings
from .utils import encode_base64


@pytest.fixture
def solution_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/solution.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


@pytest.fixture
def tester_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/tester.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


async def test_rest_validation_json_error(api_client):
    response = await api_client.post("/api/validate", data="TEST")
    data = await response.json()

    assert "error" in data, data
    assert data["error"] == "Request is malformed; could not decode JSON object."


async def test_rest_validation_error_empty_payload(api_client):
    response = await api_client.post("/api/validate", json={})
    data = await response.json()

    assert "error" in data, data
    assert "errors" in data
    assert data["error"] == "Validation error: data must contain ['files'] properties"


async def test_rest_production_callback_missing(api_client, monkeypatch, solution_fixture, tester_fixture):
    monkeypatch.setattr(settings, "DEBUG", False)
    response = await api_client.post(
        "/api/validate",
        json={
            "files": {
                "solution.py": solution_fixture,
                "tester.py": tester_fixture,
            },
            "solution_function": "add",
        },
    )
    data = await response.json()

    assert "error" in data, data
    assert data["error"] == "Validation error: data must contain ['files', 'callback_url'] properties"


async def test_websocket_validation_json_error(api_client):
    async with api_client.ws_connect("/api/validate/ws") as ws:
        await ws.send_str("TEST")
        data = await ws.receive_json()

    assert "error" in data, data
    assert data["error"] == "Request is malformed; could not decode JSON object."


async def test_websocket_validation_error_empty_payload(api_client):
    async with api_client.ws_connect("/api/validate/ws") as ws:
        await ws.send_json({})
        data = await ws.receive_json()

    assert "error" in data, data
    assert "errors" in data
    assert data["error"] == "Validation error: data must contain ['files'] properties"
