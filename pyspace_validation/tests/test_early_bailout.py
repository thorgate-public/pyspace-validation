import asyncio

import pytest

from .utils import encode_base64


@pytest.fixture
def solution_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/solution.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


@pytest.fixture
def tester_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/tester.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


async def test_websocket_early_bailout(api_client, solution_fixture, tester_fixture):
    async def handle_fail():
        await asyncio.sleep(10)
        pytest.fail("Expected to be cancelled.")

    failure_task = asyncio.create_task(handle_fail())

    async with api_client.ws_connect("/api/validate/ws") as ws:
        await ws.send_json(
            {
                "files": {
                    "solution.py": solution_fixture,
                    "tester.py": tester_fixture,
                },
                "solution_function": "add_sleep",
            }
        )

        await asyncio.sleep(3)
        await ws.close()

    failure_task.cancel()
