import uuid

import pytest

from pyspace_validation.orchestrator.utils import code_directory, write_files
from pyspace_validation.tests.utils import encode_base64, override_setting


@pytest.fixture
def solution_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/solution.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


@pytest.fixture
def tester_fixture(fixture_dir):
    with open(f"{fixture_dir}/addition/tester.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


async def test_checker_write_files_mounts_nested_directories(solution_fixture, tester_fixture):
    async with code_directory(str(uuid.uuid4())) as temp_dir:
        mounts = await write_files(
            temp_dir=temp_dir,
            files={
                "addition/solution.py": solution_fixture,
                "addition/nested/directory/solution2.py": solution_fixture,
                "addition/../addition/tester.py": tester_fixture,
            },
        )

    assert set(mounts) == {
        f"{temp_dir.full_dir}/addition/nested/directory/solution2.py:/src/addition/nested/directory/solution2.py:ro",
        f"{temp_dir.full_dir}/addition/solution.py:/src/addition/solution.py:ro",
        f"{temp_dir.full_dir}/addition/tester.py:/src/addition/tester.py:ro",
    }


async def test_checker_write_files_host_mount_override(solution_fixture, tester_fixture):
    with override_setting("DOCKER_HOST_DIR", "/path/to/host/dir"):
        async with code_directory(str(uuid.uuid4())) as temp_dir:
            mounts = await write_files(
                temp_dir=temp_dir,
                files={
                    "addition/solution.py": solution_fixture,
                    "addition/nested/directory/solution2.py": solution_fixture,
                    "addition/../addition/tester.py": tester_fixture,
                },
            )

        assert set(mounts) == {
            f"{temp_dir.host_dir}/addition/nested/directory/solution2.py:/src/addition/nested/directory/solution2.py:ro",
            f"{temp_dir.host_dir}/addition/solution.py:/src/addition/solution.py:ro",
            f"{temp_dir.host_dir}/addition/tester.py:/src/addition/tester.py:ro",
        }


async def test_checker_write_files_mounts_invalid_path(solution_fixture):
    async with code_directory(str(uuid.uuid4())) as temp_dir:
        with pytest.raises(Exception, match="Invalid input paths"):
            await write_files(
                temp_dir=temp_dir,
                files={
                    "../../../../../etc/sshd.conf": solution_fixture,
                },
            )
