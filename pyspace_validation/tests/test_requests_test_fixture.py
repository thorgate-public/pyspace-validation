import pytest

from aioresponses import aioresponses
from aioresponses.compat import URL

from pyspace_validation.types import ResultStatus

from .utils import encode_base64


@pytest.fixture
def solution_fixture(fixture_dir):
    with open(f"{fixture_dir}/requests-test/solution.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


@pytest.fixture
def tester_fixture(fixture_dir):
    with open(f"{fixture_dir}/requests-test/tester.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


async def test_rest_run_requests_tester(api_client, solution_fixture, tester_fixture):
    response = await api_client.post(
        "/api/validate",
        json={
            "files": {
                "solution.py": solution_fixture,
                "tester.py": tester_fixture,
            },
            "solution_function": "http_bin_post",
            "requirements": encode_base64("requests\n"),
            "network_allowed": True,
        },
    )
    result = await response.json()

    assert "status" in result, result
    assert result["status"] == ResultStatus.CORRECT_RESULT
    assert result["summary"]["failed"] == 0
    assert result["summary"]["error"] == 0
    assert result["summary"]["passed"] == 2
    assert result["summary"]["total"] == 2

    tests_results = sorted(
        [{key: value for key, value in tests.items() if key != "duration"} for tests in result["summary"]["tests"]],
        key=lambda value: value["name"],
    )

    assert tests_results == [
        {"name": "test_payload_dict", "outcome": "passed"},
        {"name": "test_payload_list", "outcome": "passed"},
    ]


async def test_rest_run_requests_tester_with_callback(api_client, solution_fixture, tester_fixture):
    with aioresponses(
        passthrough=[
            "http://127.0.0.1",
            "http://docker",
            "unix:///run/docker.sock",
            "unix://localhost",
            "tcp://docker",
        ]
    ) as m:
        m.post("https://example.com/validation/1")
        response = await api_client.post(
            "/api/validate",
            json={
                "files": {
                    "solution.py": solution_fixture,
                    "tester.py": tester_fixture,
                },
                "solution_function": "http_bin_post",
                "requirements": encode_base64("requests\n"),
                "network_allowed": True,
                "callback_url": "https://example.com/validation/1",
            },
        )
        data = await response.json()

        assert data["accepted"] is True

        await api_client.server.app["validation_queue"].join()

        request = m.requests[("POST", URL("https://example.com/validation/1"))][0]
        result = request[1]["json"]

    assert "status" in result, result
    assert result["status"] == ResultStatus.CORRECT_RESULT
    assert result["summary"]["failed"] == 0
    assert result["summary"]["error"] == 0
    assert result["summary"]["passed"] == 2
    assert result["summary"]["total"] == 2

    tests_results = sorted(
        [{key: value for key, value in tests.items() if key != "duration"} for tests in result["summary"]["tests"]],
        key=lambda value: value["name"],
    )

    assert tests_results == [
        {"name": "test_payload_dict", "outcome": "passed"},
        {"name": "test_payload_list", "outcome": "passed"},
    ]


async def test_websocket_run_requests_tester(api_client, solution_fixture, tester_fixture):
    async with api_client.ws_connect("/api/validate/ws") as ws:
        await ws.send_json(
            {
                "files": {
                    "solution.py": solution_fixture,
                    "tester.py": tester_fixture,
                },
                "solution_function": "http_bin_post",
                "requirements": encode_base64("requests\n"),
                "network_allowed": True,
            }
        )
        result = await ws.receive_json()

    assert "status" in result, result
    assert result["status"] == ResultStatus.CORRECT_RESULT
    assert result["summary"]["failed"] == 0
    assert result["summary"]["error"] == 0
    assert result["summary"]["passed"] == 2
    assert result["summary"]["total"] == 2

    tests_results = sorted(
        [{key: value for key, value in tests.items() if key != "duration"} for tests in result["summary"]["tests"]],
        key=lambda value: value["name"],
    )

    assert tests_results == [
        {"name": "test_payload_dict", "outcome": "passed"},
        {"name": "test_payload_list", "outcome": "passed"},
    ]
