import os

import pytest

from pyspace_validation.application import app_factory


@pytest.fixture
def event_loop(loop):
    # Make pytest-asyncio use pytest-aiohttp event loop
    return loop


@pytest.fixture
async def api_client(aiohttp_client):
    app = app_factory()
    return await aiohttp_client(app)


@pytest.fixture
def fixture_dir():
    return os.path.join(os.path.dirname(__file__), "fixtures")
