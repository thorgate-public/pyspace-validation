import base64
from contextlib import contextmanager
from unittest.mock import patch


def encode_base64(value: str) -> str:
    return base64.b64encode(value.encode("utf-8")).decode("utf-8")


@contextmanager
def override_setting(key, value):
    with patch(f"pyspace_validation.settings.{key}", value):
        yield
