import pytest

from aioresponses import aioresponses
from aioresponses.compat import URL

from pyspace_validation.types import ResultStatus

from .utils import encode_base64


@pytest.fixture
def solution_fixture(fixture_dir):
    with open(f"{fixture_dir}/pytest-based/solution.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


@pytest.fixture
def tester_fixture(fixture_dir):
    with open(f"{fixture_dir}/pytest-based/tester.py", encoding="utf-8") as fp:
        return encode_base64(fp.read())


def _validate_result(result):
    assert "status" in result, result
    assert result["status"] == ResultStatus.CORRECT_RESULT
    assert result["summary"]["failed"] == 0
    # 4 from tester + 1 from solution
    assert result["summary"]["passed"] == 5
    assert result["summary"]["total"] == 5

    tests_results = sorted(
        [{key: value for key, value in tests.items() if key != "duration"} for tests in result["summary"]["tests"]],
        key=lambda value: value["name"],
    )

    assert tests_results == [
        {"name": "test_currency_sign_euro", "outcome": "passed"},
        {"name": "test_test_fails_with_invalid_function_pound", "outcome": "passed"},
        {"name": "test_test_fails_with_invalid_function_usd", "outcome": "passed"},
        {"name": "test_test_passes", "outcome": "passed"},
        {"name": "test_test_passes_with_eur", "outcome": "passed"},
    ]


async def test_rest_run_nested_pytest_no_callback(api_client, solution_fixture, tester_fixture):
    response = await api_client.post(
        "/api/validate",
        json={
            "files": {
                "solution.py": solution_fixture,
                "tester.py": tester_fixture,
            },
            "requirements": encode_base64("pytest~=6.0\n"),
            "solution_function": "currency_sign_from_name",
        },
    )
    result = await response.json()

    _validate_result(result)


async def test_rest_run_nested_pytest_with_callback(api_client, solution_fixture, tester_fixture):
    with aioresponses(
        passthrough=[
            "http://127.0.0.1",
            "http://docker",
            "unix:///run/docker.sock",
            "unix://localhost",
            "tcp://docker",
        ]
    ) as m:
        m.post("https://example.com/validation/1")
        response = await api_client.post(
            "/api/validate",
            json={
                "files": {
                    "solution.py": solution_fixture,
                    "tester.py": tester_fixture,
                },
                "requirements": encode_base64("pytest~=6.0\n"),
                "solution_function": "currency_sign_from_name",
                "callback_url": "https://example.com/validation/1",
            },
        )
        data = await response.json()

        assert data["accepted"] is True

        await api_client.server.app["validation_queue"].join()

        request = m.requests[("POST", URL("https://example.com/validation/1"))][0]
        result = request[1]["json"]

    _validate_result(result)


async def test_websocket_run_nested_pytest(api_client, solution_fixture, tester_fixture):
    async with api_client.ws_connect("/api/validate/ws") as ws:
        await ws.send_json(
            {
                "files": {
                    "solution.py": solution_fixture,
                    "tester.py": tester_fixture,
                },
                "requirements": encode_base64("pytest~=6.0\n"),
                "solution_function": "currency_sign_from_name",
            }
        )
        result = await ws.receive_json()

    _validate_result(result)
