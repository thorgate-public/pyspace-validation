async def test_health_check_passes(api_client):
    response = await api_client.get("/_health")

    data = await response.json()

    assert "docker" in data
    assert data["docker"]["status"] is True
    assert data["docker"]["reason"] is None


async def test_health_check_fails(api_client, monkeypatch):
    monkeypatch.setenv("DOCKER_HOST", "http://invalid:2037")

    response = await api_client.get("/_health")

    data = await response.json()

    assert "docker" in data
    assert data["docker"]["status"] is False
    assert data["docker"]["reason"] == "Cannot connect to docker engine"
