from dataclasses import dataclass
from enum import IntEnum
from pathlib import Path
from typing import Any, Optional, TypedDict


class ResultStatus(IntEnum):
    SUBMITTED = 0
    IN_PROGRESS = 10
    TIMEOUT = 15
    WRONG_RESULT = 20
    CORRECT_RESULT = 30


ValidationPayloadFiles = dict[str, str]

ValidationReport = dict[str, Any]

ContainerStats = list[dict[str, Any]]


class ValidationSummaryTests(TypedDict):
    name: str
    outcome: str

    duration: float


class ValidationSummary(TypedDict):
    failed: int
    error: int
    passed: int
    total: int

    duration: float

    tests: list[ValidationSummaryTests]


class ValidationPayload(TypedDict):
    files: ValidationPayloadFiles
    solution_function: str
    requirements: Optional[str]
    network_allowed: bool
    timeout: float

    callback_url: Optional[str]


class ValidationResult(TypedDict):
    status: ResultStatus

    reason: Optional[str]

    report: Optional[ValidationReport]

    summary: Optional[ValidationSummary]

    stats: Optional[ContainerStats]

    logs: Optional[list[str]]


class ValidationQueuePayload(TypedDict):
    hash_id: str
    payload: ValidationPayload
    callback_url: str


@dataclass
class CodeDirectory:
    base_dir: str
    full_dir: Path
    host_dir: Optional[Path]
