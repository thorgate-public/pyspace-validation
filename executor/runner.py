import os
import sys

import pytest

from jwt import encode
from pytest_jsonreport.plugin import JSONReport


if __name__ == "__main__":
    SECRET_KEY = os.environ.pop("SECRET_KEY")
    test_function = os.getenv("TEST_FUNCTION", "")

    if len(sys.argv) != 2:
        print("ERROR: Missing arguments <timeout>")
        sys.exit(1)

    timeout = sys.argv[1]

    plugin = JSONReport()
    exit_code = pytest.main(
        [
            "--json-report-file=none",
            "--json-report-omit=collectors,log,traceback,streams,warnings",
            f"--pyspace-test-function={test_function}",
            f"--timeout={timeout}",
            "-vv",
        ],
        plugins=[plugin],
    )

    print(f"Pytest finished with exit code: {exit_code}")

    # signed report with JWT
    print("+------<<<<        TEST REPORT        >>>>------+")
    print(encode(plugin.report, SECRET_KEY))
