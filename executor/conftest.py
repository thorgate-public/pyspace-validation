import importlib
import os
from unittest import mock

import pytest


def pytest_addoption(parser):
    group = parser.getgroup("pyspace")
    group.addoption(
        "--pyspace-test-function",
        action="store",
        dest="solution_function",
    )


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ, {"SECRET_KEY": "nice-try"}):
        yield


@pytest.fixture
def solution_module():
    return importlib.import_module("solution")


@pytest.fixture
def solution_function(request: pytest.FixtureRequest, solution_module):
    name = request.config.getoption("solution_function", None)

    if name:
        return getattr(solution_module, name)

    return None
